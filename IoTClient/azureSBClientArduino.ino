// Configuracion de los pines del potenciometro y del LED
int potPin = 0;
int redPin= 7;
int greenPin = 6;
int bluePin = 5;
// Valor del potenciometro
int val = 0;

void setup(){
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  pinMode(8, OUTPUT);
  
}

void loop(){
  val = analogRead(potPin);
  val = map(val, 0, 1023, 0, 255);
  Serial.println(val);
  if(Serial.available()){
    char serialListener = Serial.read();
    if (serialListener == 'H') {
        setColor(0, 0, 255);
        delay(500);
        setColor(0, 0, 255);
        delay(500);
     }
    else if(serialListener == 'E'){
      setColor(255, 0, 0);
      delay(500); 
      setColor(0, 0, 0);  
      delay(500);
    }
    else {
      setColor(0, 0, 0);
    }
  }
}


// Enciende el led RGB en un color especifico
void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}


