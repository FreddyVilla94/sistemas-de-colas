__author__ = 'Anthony'

import requests
import serial

arduino = serial.Serial('/dev/ttyACM0', 9600)
API_ENDPOINT = 'https://api-team-operating-system.herokuapp.com/api'

data = {
    'numberOfRequest': 1
}


def castNumber(string):
    number = ''
    for c in string:
        if c in '0123456789':
            number += c
    try:
        return int(number)
    except:
        return 0    

while True:
    arduino.write('X')
    potValue = arduino.readline()
    potValue = castNumber(potValue)
    print 'Valor del potenciometro ' + str(potValue)
    # Envia mensajes a la cola mientras el valor del potenciometro see mayor o igual a 100
    if int(potValue) >= 100:
        print '[*Info] Enviando mensaje al API'
        r = requests.post(url=API_ENDPOINT, data=data)
        print '[*Info] Respuesta del API ' + r.text
        if r.text == 'true':
            # Enciende el led en azul si el post fue exitoso
            arduino.write('H')
        else:
            # Enciendo el led en en rojo si hubo algun error
            arduino.write('E')    



