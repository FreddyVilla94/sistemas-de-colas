package com.freddy.projects.clientemovil.API;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiTeamsOperatingSystem {

    //Send Post Number Messages
    @POST("api")
    Call<Boolean> sendNumberMessages(@Body Map<String, String> object);
}
