package com.freddy.projects.clientemovil;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.freddy.projects.clientemovil.API.RetrofitClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    RetrofitClient retrofitClient;
    EditText numberOfRequets;
    Button btnSend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        retrofitClient =  new RetrofitClient();
        numberOfRequets = (EditText)  findViewById(R.id.editNumero);
        btnSend = (Button) findViewById(R.id.btnEnviar);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numberOfRequestS =  numberOfRequets.getText().toString();
                if (numberOfRequestS.equals("")){
                    Toast.makeText(getApplicationContext(), "Datos Vacios", Toast.LENGTH_SHORT).show();
                } else {
                    sendMessage(numberOfRequestS);
                }
            }
        });

    }

    private void sendMessage(String numberOfRequestS) {
        Map<String,String> datos = new HashMap<>();
        datos.put("numberOfRequest",numberOfRequestS);
        try {
            this.retrofitClient.getService().sendNumberMessages(datos).enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    if (response.isSuccessful()){
                        Toast.makeText(getApplicationContext(), response.body().toString(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "No funco", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }
}
