package com.freddy.projects.clientemovil.API;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private ApiTeamsOperatingSystem service;
    private final Retrofit retrofit;
    private String API_URL = "https://api-team-operating-system.herokuapp.com/";

    public RetrofitClient(){
        this.retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.service = retrofit.create(ApiTeamsOperatingSystem.class);
    }

    public ApiTeamsOperatingSystem getService() {
        return service;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public String getAPI_URL() {
        return API_URL;
    }

}
