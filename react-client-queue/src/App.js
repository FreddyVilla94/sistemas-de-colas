import React, { Component } from 'react';
import './App.css';
import Team from './Team/Team';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Team />
      </div>
    ); 
  }
}

export default App;
