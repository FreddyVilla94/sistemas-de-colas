import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import Grid from '@material-ui/core/Grid';
import axios from 'axios';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 230
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  demo: {
    height: '99.5vh'
  },
});

class TextFields extends React.Component {
  state = {
    numberOfRequest: ''
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  sendNumberOfRequests = () => {
    axios.post('https://api-team-operating-system.herokuapp.com/api', this.state)
      .then(res => {
        console.log(res.data);
      })
  }

  render() {
    const { classes } = this.props;

    return (
      <Grid container spacing={24}
        className={classes.demo}
        alignItems='center'
        direction='row'
        justify = 'center'>
      <form className={classes.container} noValidate autoComplete="off">
        <Grid item xs={12}>
          <TextField
            id="number"
            label="Ingrese el número de peticiones"
            value={this.state.numberOfRequest}
            onChange={this.handleChange('numberOfRequest')}
            type="number"
            className={classes.textField}
            margin="normal"
          />
        </Grid>
        <Grid item xs={12}>
          <Button onClick={ this.sendNumberOfRequests }
            variant="contained"
            color="primary"
            display="block"
            className={classes.button}>
              Enviar
            <Icon className={classes.rightIcon}>send</Icon>
          </Button>
        </Grid>
      </form>
      </Grid>
    );
  }
}

TextFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TextFields);