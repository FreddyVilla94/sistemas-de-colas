Servidores de Colas
III Proyecto
 
Freddy Villalobos Gonz�lez & Anthony Segura Chavez & Michael M�ndez Montoya
 
Instituto Tecnol�gico de Costa Rica
Escuela de Ingenier�a en Computaci�n
Principios de Sistemas Operativos

¿Qué es Azure?
Azure es una nube pública de pago por uso que permite compilar, implementar y administrar rápidamente aplicaciones en una red global de data centers de Microsoft.

Implementaci�n del servicio de cola

Mobile

El cliente m�vil es una aplicaci�n elaborada en Android Studio, la cual �nicamente le solicita el n�mero de mensajes que desea enviar el usuario al servicio de cola alojado
en azure, una vez ingresado el n�mero de mensajes, se presiona el boton y se envia  el numero de mensajes por POST a un api, el cual esta alojado en heroku, el api realiza la 
construcci�n de los mensajes y se insertan a la cola en azure. El lenguaje utilizado para el desarrollo del cliente m�vil es Java.

Desktop

El cliente de Escritorio es una aplicaci�n elaborada en Visual Studio 2017,  la cual �nicamente le solicita el n�mero de mensajes que desea enviar el usuario al servicio de cola 
alojado en azure, una vez ingresado el n�mero de mensajes, se presiona el bot�n y se envia  el numero de mensajes por POST a un api, el cual esta alojado en heroku, el api realiza la 
construcci�n de los mensajes y se insertan a la cola en azure. El lenguaje utilizado para el desarrollo del cliente m�vil es C#.

Web

El cliente Web contiene un pequeño formulario que le solicita el número de mensajes que desea enviar el usuario al servicio de cola alojado en azure, una vez ingresado el número de mensajes, se presiona el botón y se envia  el numero de mensajes por POST a un api, el cual esta alojado en Heroku. El api realiza la construcción de los mensajes y se insertan a la cola en azure. Se utiliza JavaScript como lenguaje de programación y la librería React.js.

IoT

El cliente IoT cuenta con dos componentes principales, una placa Arduino Uno la cual sirve como dispositivo de entrada y salida, a esta placa están conectados un potenciómetro y un led RGB. La placa de Arduino se comunica al segundo componente un Raspberry Pi 2 mediante comunicación serial usb. El dispositivo Raspberry corre un pequeño script de Python 2.7 que lee el valor del potenciómetro del Arduino dentro de un loop, cuando este valor supera 100 se enviá un mensaje al api para que inserte mensajes en la cola, si el api responde con un mensaje de éxito se enciende el led del Arduino de color azul, en caso de un mensaje de error se enciende en rojo. Una vez que el valor del potenciómetro baja del umbral de 100 se dejan de enviar mensajes a la cola y se apaga el led. Para este cliente es necesario instalar las librerías pySerial y requests para Python 2.7 y contar con una conexión a internet en el Raspberry Pi ya se mediante conexión cableada o Wifi.

Backend SQL Server y MySql

MySql

https://docs.microsoft.com/en-us/azure/mysql/quickstart-create-mysql-server-database-using-azure-portal

1. Seleccione el bot�n Crear un recurso (+) en la esquina superior izquierda del portal. Seleccione Bases de datos > Base de datos de Azure para MySQL . Tambi�n puede escribir MySQL 
en el cuadro de b�squeda para encontrar el servicio.


2. Complete el nuevo formulario de detalles del servidor con la siguiente informaci�n:

3. Seleccione Crear para aprovisionar el servidor. El aprovisionamiento puede tomar hasta 20 minutos.

4. En la p�gina del servidor, seleccione Seguridad de conexi�n. En el encabezado de reglas del firewall , seleccione el cuadro de texto en blanco en la columna Nombre de la regla para 
comenzar a crear la regla del firewall.

SQL Server

https://docs.microsoft.com/en-us/azure/sql-database/sql-database-get-started-portal

1. Haga clic en Crear un recurso en la esquina superior izquierda del portal de Azure. Seleccione Bases de datos en la p�gina Nueva y seleccione Crear en Base de datos SQL en la p�gina 
Nueva.

2. Complete el formulario de la base de datos SQL con la siguiente informaci�n, como se muestra en la imagen.

3. Una vez completada la implementaci�n, haga clic en bases de datos SQL en el men� de la izquierda y luego haga clic en mySampleDatabase en la p�gina de bases de datos SQL. 

4. Haga clic en Establecer servidor de seguridad en la barra de herramientas como se muestra en la imagen anterior. Se abre la p�gina de configuraci�n del servidor de seguridad para 
el servidor de la base de datos SQL.

Servidor de Colas.

https://docs.microsoft.com/es-es/azure/service-bus-messaging/service-bus-dotnet-get-started-with-queues

https://docs.microsoft.com/es-es/azure/service-bus-messaging/service-bus-azure-and-service-bus-queues-compared-contrasted

En azure existen dos tipos de mecanismos de cola: colas de Storage y colas de Service Bus.
Las colas de Storage, que forman parte de la infraestructura de Azure Storage, ofrecen una interfaz de GET/PUT/PEEK sencilla basada en REST, que ofrece una mensajería confiable y persistente dentro de los servicios y entre ellos.
Las colas de Service Bus forman parte de una infraestructura de mensajería de Azure más amplia que admite la puesta en cola, así como la publicación/suscripción, y patrones de integración más avanzados.
Este proyecto utiliza el mecanismo de cola Service Bus debido a las facilidades que ofrece con respecto a las colas de Storage, ya que estas últimas se utilizan si se quiere almacenar grandes cantidades de datos (más de 80 GB).
 
Pasos para crear una nueva cola de Service Bus:
1. Haga clic en Todos los recursos y, después, en el nombre del espacio creado.
2. En la ventana del espacio de nombres, haga clic en Directivas de acceso compartido.
3. En la pantalla Directivas de acceso compartido, haga clic en RootManageSharedAccessKey.
4. En la ventana Directiva: RootManageSharedAccessKey, haga clic en el botón Copiar que hay junto a Cadena de conexión: clave principal para copiar la cadena de conexión en el portapapeles para su uso posterior. Pegue este valor en el Bloc de notas o cualquier otra ubicación temporal.
5. Repita el paso anterior, copie y pegue el valor de clave principal en una ubicación temporal para su uso posterior.
6. En la ventana del espacio de nombres, haga clic en Colas y, después, en la ventana Colas, haga clic en + Cola.
7. Escriba el nombre de la cola y deje los restantes valores con sus valores predeterminados.
8. En la parte inferior de la ventana, haga clic en Crear.



